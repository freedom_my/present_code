<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Answer;
use app\modules\admin\models\QuestionSearch;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii;
use app\modules\admin\models\Question;

class QuestionController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                        'denyCallback' => function($rule, $action) {
                            return $this->redirect(Url::toRoute(['/admin/users/login']));
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->isGuest;
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionAdd()
    {
        $question =  new Question();
        for ($i = 0; $i < 4; $i++) {
            $answers[] = new Answer();
        }

        if ($question->load(Yii::$app->request->post()) && $question->save()) {
           foreach ($answers as $key => $answer) {
               if ($answer->load(['Answer'=>Yii::$app->request->post()['Answer'][$key]])){
                   $answer->question_id = $question->getId();
                   if ($answer->validate()) {
                       $answer->save();
                   }
               }
           }
            return $this->redirect(['update', 'id' => $question->getId()]);
        }

        return $this->render('add', [
            'question' => $question,
            'answers' => $answers
        ]);
    }

    public function actionList()
    {
        $searchModel = new QuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id = null)
    {
        $question = $this->findModel($id);
        $answers = $this->findQuestions($question->getId());

        if ($question->load(Yii::$app->request->post()) && $question->save()) {
            foreach ($answers as $key => $answer) {
                if ($answer->load(['Answer'=>Yii::$app->request->post()['Answer'][$key]])){
                    $answer->question_id = $question->getId();
                    if ($answer->validate()) {
                        $answer->save();
                    }
                }
            }
            return $this->redirect(['update', 'id' => $question->id]);
        } else {
            return $this->render('update', [
                'question' => $question,
                'answers' => $answers
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Question::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findQuestions($question_id)
    {
        if (($answers = Answer::find()->where(['question_id'=>$question_id])->all()) !== null) {
            return $answers;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
<?php

namespace app\modules\admin\models;

class Answer extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'answers';
    }

    public function rules(){
        return [
            [['question_id'],'required'],
            [['correct'],'default'],
            [['text'],'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'correct' => 'Правильный',
            'text' => 'Текст ответа'
        ];
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }
}
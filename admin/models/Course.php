<?php
namespace app\modules\admin\models;

use yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;

class Course extends ActiveRecord
{
    public static function tableName()
    {
        return 'courses';
    }

    public function rules(){
        return [
            [['name','text','document_name'],'required'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название курса',
            'text' => 'Описание курса',
            'document_name' => 'Название документа',
            'lesson' => 'Ссылка на уроки',
        ];
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }


}
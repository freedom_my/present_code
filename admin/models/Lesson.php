<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class Lesson extends ActiveRecord
{

    public static function tableName()
    {
        return 'lessons';
    }

    public function rules(){
        return [
            [['text','course_id'],'required'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Курс',
            'text' => 'Название урока',
            'theme' => 'Темы'
        ];
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }



}
<?php

namespace app\modules\admin\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;

class LessonSearch extends Lesson
{

    public function rules()
    {
         return [
             [['text'],'safe'],
             [['course_id'],'default']
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Lesson::find()->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'course_id', $this->course_id]);

        return $dataProvider;
    }
}
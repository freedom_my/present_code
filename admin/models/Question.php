<?php

namespace app\modules\admin\models;

class Question extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'questions';
    }

    public function rules(){
        return [
            [['lesson_id','text'],'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lesson_id' => 'Урок',
            'text' => 'Текст вопроса'
        ];
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }
}
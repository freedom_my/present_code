<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;

class Theme extends ActiveRecord
{
    public static function tableName()
    {
        return 'themes';
    }

    public function rules(){
        return [
            [['name','title','text','lesson_id'],'default'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lesson_id' => 'Урок',
            'text' => 'Текст темы',
            'title' => 'Описание темы',
            'name' => 'Имя темы',
        ];
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }
}
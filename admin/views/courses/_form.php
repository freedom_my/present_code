<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'document_name')->textInput(['maxlength' => 255]) ?>

<!--    --><?//= $form->field($model, 'date_start')->textInput([  'value' => Yii::$app->formatter->asDatetime($model->date_start, 'php:d.m.Y'),]) ?>

<!--    --><?//= $form->field($model, 'date_end')->textInput([  'value' => Yii::$app->formatter->asDatetime($model->date_end, 'php:d.m.Y'),]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
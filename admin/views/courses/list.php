<?php
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;
?>

<h1>Список курсов</h1>

<h3><?=Html::a('Добавить курс', Url::to(['/admin/course/add/']));?></h3>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        'id',
        'name',
        'text',
        'document_name',
        [
            'attribute'=>'lesson',
            'format' => 'raw',
            'value' => function($model){
                return Html::a('Уроки', Url::to(['/admin/lesson/list/', 'LessonSearch[course_id]'=>$model->id]));
            },
        ],

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\modules\admin\assets\AdminAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Пользователи', 'items'=>[
                ['label' => 'Список пользователей', 'url' => ['/admin/users/list']],
                ['label' => 'Добавить пользователя', 'url' => ['/admin/users/add']],
            ]],
            ['label' => 'Курсы', 'items'=>[
                ['label' => 'Список курсов', 'url' => ['/admin/courses/list']],
                ['label' => 'Добавить курс', 'url' => ['/admin/courses/add']],
            ]],
            ['label' => 'Уроки', 'items'=>[
                ['label' => 'Список уроков', 'url' => ['/admin/lesson/list']],
                ['label' => 'Добавить урок', 'url' => ['/admin/lesson/add']],
            ]],
            ['label' => 'Темы', 'items'=>[
                ['label' => 'Список тем', 'url' => ['/admin/theme/list']],
                ['label' => 'Добавить тему', 'url' => ['/admin/theme/add']],
            ]],
            ['label' => 'Статистика', 'url' => ['/admin/stats/']],
            !Yii::$app->user->isGuest ? (
                '<li>'
                . Html::beginForm(['/admin/users/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Выйти из админки ',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            ) : ''
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <?= Alert::widget() ?>
    <div class="container">
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">FreeDom <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

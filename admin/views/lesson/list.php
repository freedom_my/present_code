<?php
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        'id',
        'course_id',
        'text',
        [
            'attribute'=>'theme',
            'format' => 'raw',
            'value' => function($model){
                return Html::a('Темы', Url::to(['/admin/theme/list/', 'ThemeSearch[lesson_id]'=>$model->id]));
            },
        ],

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
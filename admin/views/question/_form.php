<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($question, 'text')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($question, 'lesson_id')->textInput(['maxlength' => 255]) ?>

    <? foreach ($answers as $key => $answer) :?>
        <div class="d-flex flex-lg-row">
            <?= $form->field($answer, "[$key]text",['options'=>['class'=>'flex-grow-1']])->textarea(['rows'=>1]) ?>
            <?= $form->field($answer, "[$key]correct")->checkbox() ?>
        </div>
    <? endforeach;?>
    <div class="form-group">
        <?= Html::submitButton($question->isNewRecord ? 'Create' : 'Update', ['class' => $question->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

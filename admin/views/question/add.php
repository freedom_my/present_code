<?php

use yii\helpers\Html;
?>
<div class="product-update">

    <h1>Добавление вопроса</h1>

    <?= $this->render('_form', [
        'question' => $question,
        'answers' => $answers,
    ]) ?>

</div>
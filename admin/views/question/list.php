<?php
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;
?>
    <h1>Список Вопросов</h1>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        'id',
        'lesson_id',
        'text',
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
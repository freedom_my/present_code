<?php

use yii\helpers\Html;
?>
<div class="product-update">

    <h1>Изменение вопроса</h1>

    <?= $this->render('_form', [
        'question' => $question,
        'answers' => $answers,
    ]) ?>

</div>

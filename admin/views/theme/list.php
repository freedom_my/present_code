<?php
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;
?>
<h1>Список тем</h1>

<h3><?=Html::a('Добавить тему', Url::to(['/admin/theme/add/']));?></h3>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        'id',
        'lesson_id',
        'name',
        'title',
        [
            'attribute'=>'text',
            'format' => 'raw',
        ],

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
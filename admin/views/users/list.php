<?php
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;
?>
<h1>Список пользователей</h1>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        'id',
        'username',
        'email',
    ],
]); ?>
